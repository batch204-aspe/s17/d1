//console.log("hello");

/*
	FUNCTIONS
		►	functions in JS are lines/block of codes that tell our device/application to perform a certain task when called/invoked.
		
		►	functions are mostly created to create complicated tasks to run several lines of codes.

	Function Declarations
		► functions statement defines a function with  a specified parameters

		SYNTAX:
				function functionName () {
						code block(statement);

				};

		function keyword ► used to define a javaScript functions
		functionName ► the functionNAme. Functions are named to be able to use later in the code.
		function block ( {} ) ► the statement which comprise the body of the function. This is where the code to be executed.

*/

function printName(){
	console.log("My name is John");
};


/*
	Function Invocation
		► The Code Block & statements inside a function is not immediately executed when the function is defined. The code block & statements inside a function is executed when the function is invoked or called.
*/

printName(); // invoked/called a function in order to be executed

	// declaredFunction(); // error - because we cannot invoke a function we have yet to define

// Function Declaration vs Function Expression
	
	
/*
	Function Declaration
	
		► declared Functions can be hoisted. As long as the function has been declared

		► hoisted ► calling/invoking a function before declared

		example:
			
			declaredFunction(); // Invoking/calling the function declared
			
			// Function Daclaration

			function declaredFunction () {
				console.log("Hello, from the declaredFunction");
			};
*/
	
	declaredFunction(); 
	function declaredFunction () {
		console.log("Hello, from the declaredFunction");
	};


/*
	Function Expression

		► A function can be also stored in a variable. THis is called a function expression.

		► is an anonymous funcion assign to the variableFunction

		► also Function Expression cannot be hoisted unlike to function declarion

	Anonymous Function - a function without a name.

*/
	
	let variableFunction = function(){
		console.log("Hello Again!");
	};

	variableFunction()

	let funcExpression = function funcName(){
		console.log("Hello form the other side!");
	};

	funcExpression();



	// Declared Function can also work in reassigning the value
	declaredFunction = function(){
		console.log("Updated declaredFunction");
	};
	declaredFunction();

	//Function Expresseion can also work in reassigning the value
	funcExpression = function(){
		console.log("Updated funcExpression");
	};
	funcExpression();


	const constantFunc = function(){
		console.log("Initialized with const");
	};
	constantFunc();

	/* 
	    Reassigning a constant variable of function reassigning is not possible 

	    i.e.
			constantFunc = function(){
				console.log("Can we reassign?");
			}
			constantFunc(); 

		error : Uncaught TypeError: Assignment to constant variable.

	*/

	/*
		Function Scoping
			3 Type of Scope in JavaScript
				1. local/block scope
				2. global scope
				3. function scope
			
			{
				
				let localVar = "Juan Dela Cruz";

			}

			let globalVar = "Mr. Worldwide";
			
	*/

	function showNames(){

		// note: these 3 variables especially the VAR Scope cannot be accessed outside the declared function

		var functionVar = "Mark";
		const functionConst = "Jayson";
		let functionLet = "Arnel";



		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	};
	showNames();

	/*
		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);

		Error: Uncaught ReferenceError: functionVar is not defined
			► because these 3 variable is declared within a function declaration
		
	*/

/*
	Nested Function
*/

function myNewFunction() {

	let name = 'Gabriella';

	function nestedFunction(){
		let nestedName = "Maria";
		console.log(name);
	};

	nestedFunction();
	//console.log(nestedName); // results an error
	// nestedName Variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in
};
myNewFunction();
// nestedFunction();  results an error



// FUnction and GLobal Scoped Variables
	// Global Scoped Variable
	let globalName = "Alexandro";

	function myNewFunction2 (){
		let nameInside = "Khabib";
		console.log(globalName);
	};
	myNewFunction2();
	// console.log(nameInside); // results : Error because it is locally declared in function therefore it cannot be accessed outside the function

	// Using Alert()
	//  alert () allows us to show a small window at the top of our browser page to show information user.

	alert("Hello, JavaSrcipt");

	function showSampleAlert(){
		alert("Hello User");
	};
	showSampleAlert();

	console.log("I wiil only log in the console when the alert is dismissed");

	// Using Prompt
		// prompt () allow us to show a small window at the top of the browser to gather user input. It, much like alert(), will have the page wait until the user compltes or enters their input. The input from promt() will be returned as a String once the user dismiss the window.
	/*

		SYNTAX: 
			prompt (" <dialogInString> ");

	*/

	let samplePrompt = prompt('ENter your Name');

	console.log("Hello, " + samplePrompt);


	function printWelcomeMessage(){

		let firstName = prompt("Enter Your First Name.");
		let lastName = prompt("Enter you Last Name.");

		console.log("Hello, " + firstName + " " + lastName + "!");
		alert("Welcome to my Page");
	};
	printWelcomeMessage();

	// Function Naming Conventions
	// SHould be relevant to the task of function that is given in instruction

	function getCourses() {

		let course = ["Science 101", "MAth", "English 101"];

	}
	getCourses();

	// Avoid generic names to avoid confusion within your code
	function get() {
		let name = "Jamie";
		console.log(name);
	}
	get();

	//Avoid pointless & inappropriate function names
	function foo () {
		console.log(25%5);
	};
	foo();

	// Name your functions in small caps. Follow camelCase when naming variables and functions

	function displayCarInfo(){

		console.log("Brand : Toyota");
		console.log("Type:Sedan");
		console.log("Price: 1,500,000.00");
	}

	displayCarInfo();